# COOP 08 Salmon
This is one of a series of old ArmA missions I am revisiting. These missions tend to be quite simple and short, aiming to have a similar feel to the base game content but
taking advantage of improvements offered by ACE3.

## Overview
 - Players: 4-8
 - Mode: Co-op multiplayer
 - Mission: Search and Destroy
 - Approx. length: 1 hour 30 minutes

## Mission
AAF forces are attempting to seize Air Station Mike 26, but NATO artillery is slowing their advance. An AAF special forces team is tasked with locating and destroying the artillery before the assault is repelled.

## Installation
This repository can be cloned straight into your `mpmissions` directory. 

### Mod dependencies
The mission depends on the following mods. 
 - [ACE3](https://ace3mod.com/)

### Other mods
The mission does not depend on the following, but they may enhance your experience.
 - Task Force Arrowhead Radio _or_ ACRE

