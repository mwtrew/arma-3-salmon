// Have _unit intermittently fire at _target, at random intervals.

params ["_unit", "_target"];

[_unit, _target] spawn { 
    params ["_unit", "_target"];
    while{true} do{ 
        _unit doSuppressiveFire _target; 
        _unit setVehicleAmmo 1; 
        sleep(random [0, 20, 60]); 
    }; 
};