// The helicopter carrying the players is in position for the fast rope deployment.

// Send a radio message instructing players to fast rope.
insertHeloD sideRadio "FastRopeReadyMsg";

// Move some enemies their way.
interceptTeam0 setCurrentWaypoint[interceptTeam0, 2];
interceptCars0 setCurrentWaypoint[interceptCars0, 2];

// re-enable the ambient forces battling near the Air Station
ambientApc0G enableSimulationGlobal true;