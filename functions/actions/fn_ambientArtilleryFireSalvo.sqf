// Have the artillery fire a salvo at the Air Station, providing a clue to the objective location.

art0 setVehicleAmmo 1;
art1 setVehicleAmmo 1;

art0Grp setCurrentWaypoint[art0Grp, 2];
art1Grp setCurrentWaypoint[art1Grp, 2];