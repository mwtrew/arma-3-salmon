// The players' squad has completed their objective and reached the extraction helipad.

// Send enemy reinforcements to the helipad.
reinforcementSquad1 setCurrentWaypoint[reinforcementSquad1, 5];
reinforcementTruck2 setCurrentWaypoint[reinforcementTruck2, 2];

insertHeloD sideRadio "EnRouteToExtractMsg";

// Wait for a random amount of time, then send the helicopter on it's approach.
// disable the ambient enemies for now so they don't shoot it too much.
_extractHandle = [] spawn {
	sleep(random[60, 120, 180]);

	ambientApc0G enableSimulationGlobal false;
	ambientApc1G enableSimulationGlobal false;

	//Play the recorded helicopter extraction sequence.
	_extracthandle = execVM "extractHelo.sqf";
}