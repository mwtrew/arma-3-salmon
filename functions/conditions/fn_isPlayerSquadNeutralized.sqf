// Check if the players are all unconscious or dead.
// We're using ACE, so a group of unconscious players is also a loss condition.

({(alive _x) AND !(_x getVariable "ACE_isUnconscious")} count units squad) == 0