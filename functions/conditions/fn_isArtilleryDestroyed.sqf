// Check if the artillery has been disabled. Using 'canMove' as they are difficult to destroy completely.

(!(canMove art0) AND !(canMove art1))